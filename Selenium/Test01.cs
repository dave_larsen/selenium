﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;

namespace Selenium
{
    class Test01
    {
        IWebDriver driver;

        [SetUp]
        public void StartBrowser()
        {
            driver = new ChromeDriver(@"C:\bin\selenium\chromedriver77");
        }

        [Test]
        public void Test()
        {
            driver.Manage().Window.Size = new Size(1024, 800);
            driver.Url = "http://test3-1.outpostcentral.com/outpostwebinternal/loginmain.aspx";

            var uname = driver.FindElement(By.XPath("//*[@id=\"tbUsername\"]"));
            uname.SendKeys("dave.larsen");

            var pwd = driver.FindElement(By.XPath("//*[@id=\"tbPassword\"]"));
            pwd.SendKeys("dkach1ng");

            var loginButton = driver.FindElement(By.XPath("//*[@id=\"btLogin\"]"));
            //Assert.AreEqual("Login", loginButton.Text);
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(@"c:\temp\screenshot.png");
            loginButton.Click();

            new WebDriverWait(driver, TimeSpan.FromMilliseconds(3000))
                .Until(d => d.FindElement(By.XPath("")).Displayed);

        }

        [TearDown]
        public void CloseBrowser()
        {
            //driver.Close();
        }
    }
}
